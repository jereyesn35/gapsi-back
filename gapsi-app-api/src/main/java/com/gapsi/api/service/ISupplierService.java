/**
 * 
 */
package com.gapsi.api.service;

import com.gapsi.api.model.dto.Supplier;
import com.gapsi.api.model.dto.response.BaseResponse;
import com.gapsi.api.model.dto.response.GetSuppliersResponse;

/**
 * @author jereyesn
 *
 */
public interface ISupplierService {

	/**
	 * 
	 * @return
	 */
	BaseResponse<GetSuppliersResponse> getAllSuppliers();

	/**
	 * 
	 * @param supplier
	 * @return
	 */
	BaseResponse<String> addSupplier(Supplier supplier);

	/**
	 * 
	 * @param supplier
	 * @return
	 */
	BaseResponse<String> editSupplier(Supplier supplier);

	/**
	 * 
	 * @param id
	 * @return
	 */
	BaseResponse<String> deleteSupplier(long id);

}

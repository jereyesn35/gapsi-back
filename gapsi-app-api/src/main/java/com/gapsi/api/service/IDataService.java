/**
 * 
 */
package com.gapsi.api.service;

import com.gapsi.api.model.dto.response.AppVersionResponse;
import com.gapsi.api.model.dto.response.BaseResponse;
import com.gapsi.api.model.dto.response.UserResponse;

/**
 * @author jereyesn
 *
 */
public interface IDataService {

	/**
	 * 
	 * @return
	 */
	BaseResponse<AppVersionResponse> getAppVersion();

	/**
	 * 
	 * @return
	 */
	BaseResponse<UserResponse> getUserName();

}

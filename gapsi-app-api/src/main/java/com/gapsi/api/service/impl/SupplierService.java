/**
 * 
 */
package com.gapsi.api.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gapsi.api.dao.ISupplierDAO;
import com.gapsi.api.exception.GapsiAppException;
import com.gapsi.api.model.dto.Supplier;
import com.gapsi.api.model.dto.response.BaseResponse;
import com.gapsi.api.model.dto.response.GetSuppliersResponse;
import com.gapsi.api.service.ISupplierService;
import com.gapsi.api.util.Constants;

import lombok.extern.log4j.Log4j2;

/**
 * @author jereyesn
 *
 */
@Service
@Log4j2
public class SupplierService implements ISupplierService {

	@Autowired
	private ISupplierDAO supplierDAO;

	public BaseResponse<GetSuppliersResponse> getAllSuppliers() {
		BaseResponse<GetSuppliersResponse> response = new BaseResponse<>(false, "", new Date().getTime(), null);
		List<Supplier> suppliers = supplierDAO.getAllSuppliers();
		if (suppliers != null && !suppliers.isEmpty()) {
			response = new BaseResponse<GetSuppliersResponse>(true, null, new Date().getTime(),
					new GetSuppliersResponse(suppliers));
		}
		return response;
	}

	@Override
	public BaseResponse<String> addSupplier(Supplier supplier) {
		BaseResponse<String> response = new BaseResponse<>(false, "", new Date().getTime(), null);
		try {
			supplierDAO.addSupplier(supplier);
			response = new BaseResponse<String>(true, null, new Date().getTime(), Constants.OPERATION_MESSAGE_SUCCESS);
		} catch (GapsiAppException exception) {
			log.error(exception.getMessage());
			response.setError(exception.getLocalizedMessage());
		}
		return response;
	}

	@Override
	public BaseResponse<String> editSupplier(Supplier supplier) {
		BaseResponse<String> response = new BaseResponse<>(false, "", new Date().getTime(), null);
		try {
			supplierDAO.updateSupplier(supplier);
			response = new BaseResponse<String>(true, null, new Date().getTime(), Constants.OPERATION_MESSAGE_SUCCESS);
		} catch (GapsiAppException exception) {
			log.error(exception.getMessage());
			response.setError(exception.getLocalizedMessage());
		}
		return response;
	}

	@Override
	public BaseResponse<String> deleteSupplier(long id) {
		BaseResponse<String> response = new BaseResponse<>(false, "", new Date().getTime(), null);
		try {
			supplierDAO.deleteSupplier(id);
			response = new BaseResponse<String>(true, null, new Date().getTime(), Constants.OPERATION_MESSAGE_SUCCESS);
		} catch (GapsiAppException exception) {
			log.error(exception.getMessage());
			response.setError(exception.getLocalizedMessage());
		}
		return response;
	}

}

/**
 * 
 */
package com.gapsi.api.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gapsi.api.dao.IAppDataDAO;
import com.gapsi.api.model.dto.response.AppVersionResponse;
import com.gapsi.api.model.dto.response.BaseResponse;
import com.gapsi.api.model.dto.response.UserResponse;
import com.gapsi.api.service.IDataService;

import lombok.extern.log4j.Log4j2;

/**
 * @author jereyesn
 *
 */
@Service(value = "kidsService")
@Log4j2
public class DataService implements IDataService {

	@Autowired
	private IAppDataDAO appDataDAO;

	@Override
	public BaseResponse<AppVersionResponse> getAppVersion() {
		log.debug("Init service execution");
		return new BaseResponse<AppVersionResponse>(true, null, new Date().getTime(),
				new AppVersionResponse(appDataDAO.getVersionApp()));
	}

	@Override
	public BaseResponse<UserResponse> getUserName() {
		log.debug("Init service execution");
		return new BaseResponse<UserResponse>(true, null, new Date().getTime(),
				new UserResponse(appDataDAO.getCandidateName()));
	}

}

/**
 * 
 */
package com.gapsi.api.exception;

/**
 * @author jereyesn
 *
 */
public class GapsiAppException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2039764410621871960L;

	/**
	 * Generic exception constructor
	 */
	public GapsiAppException() {
		super();
	};

	/**
	 * 
	 * @param message
	 */
	public GapsiAppException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public GapsiAppException(String message, Throwable cause) {
		super(message, cause);
	}

}

/**
 * 
 */
package com.gapsi.api.config;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gapsi.api.controller.DataEndpoint;
import com.gapsi.api.controller.SupplierEndpoint;

/**
 * @author jereyesn
 *
 */
@Component
@ApplicationPath("/")
public class JerseyConfig extends ResourceConfig {

	@Value("${spring.application.name}")
	private String applicationName;

	@Autowired
	public JerseyConfig() {
		registerEndpoints();
	}

	private void registerEndpoints() {
		property(ServletProperties.FILTER_FORWARD_ON_404, true);
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
		property(ServerProperties.APPLICATION_NAME, applicationName);
		register(DataEndpoint.class);
		register(SupplierEndpoint.class);
		register(WadlResource.class);
		register(JacksonJaxbJsonProvider.class);
		register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO,
				LoggingFeature.Verbosity.PAYLOAD_TEXT, 10000));
	}

}
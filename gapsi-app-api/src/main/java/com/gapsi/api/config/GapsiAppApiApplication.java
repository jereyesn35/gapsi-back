package com.gapsi.api.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ComponentScan(basePackages = "com.gapsi.api", includeFilters = { @ComponentScan.Filter(Path.class),
		@ComponentScan.Filter(ApplicationPath.class) })
@EnableAsync
public class GapsiAppApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GapsiAppApiApplication.class, args);
	}
}

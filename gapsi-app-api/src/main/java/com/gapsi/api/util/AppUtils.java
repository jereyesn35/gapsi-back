/**
 * 
 */
package com.gapsi.api.util;

import java.io.InputStream;

/**
 * @author jereyesn
 *
 */
public class AppUtils {

	public static InputStream getResourceFileAsInputStream(String fileName) {
		ClassLoader classLoader = AppUtils.class.getClassLoader();
		return classLoader.getResourceAsStream(fileName);
	}

}

/**
 * 
 */
package com.gapsi.api.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jereyesn
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppData {

	String appVersion;

	String userName;

}

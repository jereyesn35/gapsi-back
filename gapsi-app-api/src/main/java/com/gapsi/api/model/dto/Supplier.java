/**
 * 
 */
package com.gapsi.api.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jereyesn
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Supplier {

	long id;
	String name;
	int status;
	String addDate;
	String address;

}

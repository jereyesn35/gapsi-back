/**
 * 
 */
package com.gapsi.api.model.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jereyesn
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SupplierData {

	List<Supplier> items;

}

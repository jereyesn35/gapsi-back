/**
 * 
 */
package com.gapsi.api.model.dto.response;

import java.util.List;

import com.gapsi.api.model.dto.Supplier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jereyesn
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetSuppliersResponse {

	List<Supplier> items;

}

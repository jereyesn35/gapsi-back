/**
 * 
 */
package com.gapsi.api.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jereyesn
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

	String userName;

}

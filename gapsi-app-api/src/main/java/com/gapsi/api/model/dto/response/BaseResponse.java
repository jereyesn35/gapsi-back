/**
 * 
 */
package com.gapsi.api.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jereyesn
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponse<T> {

	/**
	 * Code for service response
	 */
	boolean success;

	/**
	 * Server error message in case of {success = false}
	 */
	String error;

	/**
	 * Server time in milliseconds
	 */
	long milliseconds;

	/**
	 * 
	 */
	T data;

}

/**
 * 
 */
package com.gapsi.api.dao;

/**
 * @author jereyesn
 *
 */
public interface IAppDataDAO {

	/**
	 * 
	 * @return
	 */
	String getVersionApp();

	/**
	 * 
	 * @return
	 */
	String getCandidateName();

}

/**
 * 
 */
package com.gapsi.api.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gapsi.api.dao.ISupplierDAO;
import com.gapsi.api.exception.GapsiAppException;
import com.gapsi.api.model.dto.Supplier;
import com.gapsi.api.model.dto.SupplierData;
import com.gapsi.api.util.AppUtils;

import lombok.extern.log4j.Log4j2;

/**
 * @author jereyesn
 *
 */
@Repository
@Log4j2
public class SupplierDAO implements ISupplierDAO {

	private SortedMap<Long, Supplier> suppliers;

	/**
	 * 
	 */
	public SupplierDAO() {
		log.debug("Starting suppliers DAO and memory data");
		suppliers = new TreeMap<>();
		try {
			InputStream is = AppUtils.getResourceFileAsInputStream("static/suppliers-data.json");
			ObjectMapper mapper = new ObjectMapper();
			log.debug("File exists... start reading it");
			SupplierData supplierData = mapper.readValue(is, SupplierData.class);
			if (supplierData != null && !supplierData.getItems().isEmpty()) {
				for (Supplier data : supplierData.getItems()) {
					suppliers.put(data.getId(), data);
				}
			}
		} catch (IOException exception) {
			log.error(exception);
		}
		if (suppliers.isEmpty()) {
			suppliers = new TreeMap<>();
			suppliers.put(1l, new Supplier(1l, "Prueba 1", 1, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(2l, new Supplier(2l, "Prueba 2", 1, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(3l, new Supplier(3l, "Prueba 3", 1, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(4l, new Supplier(4l, "Prueba 4", 2, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(5l, new Supplier(5l, "Prueba 5", 2, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(6l, new Supplier(6l, "Prueba 6", 2, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(7l, new Supplier(7l, "Prueba 7", 2, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(8l, new Supplier(8l, "Prueba 8", 2, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(9l, new Supplier(9l, "Prueba 9", 3, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
			suppliers.put(10l, new Supplier(10l, "Prueba 10", 3, "22/06/2010", "Calle 25 #25 Col Prueba de concepto"));
		}
	}

	@Override
	public Supplier getSupplier(long id) {
		log.debug("Start get supplier operation...");
		Optional<Map.Entry<Long, Supplier>> matchData = suppliers.entrySet().stream()
				.filter(supp -> supp.getKey().equals(id)).findFirst();
		if (matchData.isPresent()) {
			return matchData.get().getValue();
		}
		return null;
	}

	@Override
	public List<Supplier> getAllSuppliers() {
		log.debug("Start get all suppliers operation...");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException exception) {
			log.error(exception);
		}
		return suppliers.values().stream().collect(Collectors.toList());
	}

	@Override
	public List<Supplier> getSuppliers(int offset, int limit) {
		log.debug("Start get all suppliers operation...");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException exception) {
			log.error(exception);
		}
		return suppliers.values().stream().collect(Collectors.toList()).subList(offset, offset + limit);
	}

	@Override
	public void addSupplier(Supplier supplier) throws GapsiAppException {
		log.debug("Start add supplier operation...");
		if (checkSupplierExistByName(supplier.getName())) {
			throw new GapsiAppException("Data found for same supplier");
		}
		supplier.setId(getMaxId() + 1l);
		suppliers.put(supplier.getId(), supplier);
	}

	@Override
	public void updateSupplier(Supplier supplier) throws GapsiAppException {
		log.debug("Start update supplier operation...");
		if (!checkSupplierExistById(supplier.getId())) {
			throw new GapsiAppException(String.format("Data not found for supplier with id %s", supplier.getId()));
		}
		suppliers.put(supplier.getId(), supplier);
	}

	@Override
	public void deleteSupplier(long id) throws GapsiAppException {
		log.debug("Start delete supplier operation...");
		if (!checkSupplierExistById(id)) {
			throw new GapsiAppException(String.format("Data not found for supplier with id %s", id));
		}
		suppliers.remove(id);
	}

	private boolean checkSupplierExistById(long id) {
		Optional<Map.Entry<Long, Supplier>> matchData = suppliers.entrySet().stream()
				.filter(supp -> supp.getKey().equals(id)).findFirst();
		return matchData.isPresent();
	}

	private boolean checkSupplierExistByName(String supplierName) {
		Optional<Map.Entry<Long, Supplier>> matchData = suppliers.entrySet().stream()
				.filter(supp -> supp.getValue().getName().equals(supplierName)).findFirst();
		return matchData.isPresent();
	}

	private long getMaxId() {
		return suppliers.lastKey();
	}

}

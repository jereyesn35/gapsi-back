/**
 * 
 */
package com.gapsi.api.dao;

import java.util.List;

import com.gapsi.api.exception.GapsiAppException;
import com.gapsi.api.model.dto.Supplier;

/**
 * @author jereyesn
 *
 */
public interface ISupplierDAO {

	/**
	 * 
	 * @param id
	 * @return
	 * @throws GapsiAppException
	 */
	Supplier getSupplier(long id) throws GapsiAppException;

	/**
	 * 
	 * @return
	 */
	List<Supplier> getAllSuppliers();

	/**
	 * 
	 * @param offset
	 * @param limit
	 * @return
	 */
	List<Supplier> getSuppliers(int offset, int limit);

	/**
	 * 
	 * @param supplier
	 * @throws GapsiAppException
	 */
	void addSupplier(Supplier supplier) throws GapsiAppException;

	/**
	 * 
	 * @param supplier
	 * @throws GapsiAppException
	 */
	void updateSupplier(Supplier supplier) throws GapsiAppException;

	/**
	 * 
	 * @param id
	 * @throws GapsiAppException
	 */
	void deleteSupplier(long id) throws GapsiAppException;

}

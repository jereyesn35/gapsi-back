/**
 * 
 */
package com.gapsi.api.dao.impl;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gapsi.api.dao.IAppDataDAO;
import com.gapsi.api.model.dto.AppData;
import com.gapsi.api.util.AppUtils;

import lombok.extern.log4j.Log4j2;

/**
 * @author jereyesn
 *
 */
@Repository
@Log4j2
public class AppDataDAO implements IAppDataDAO {

	private String appVersion;

	private String userName;

	public AppDataDAO() {
		try {
			InputStream is = AppUtils.getResourceFileAsInputStream("static/app-data.json");
			ObjectMapper mapper = new ObjectMapper();
			log.debug("File exists... start reading it");
			AppData appData = mapper.readValue(is, AppData.class);
			appVersion = appData.getAppVersion();
			userName = appData.getUserName();
		} catch (IOException exception) {
			log.error(exception);
		}
		if (appVersion == null) {
			appVersion = "0.1.0.1";
		}
		if (userName == null) {
			userName = "Candidato desconocido";
		}
	}

	@Override
	public String getVersionApp() {
		return this.appVersion;
	}

	@Override
	public String getCandidateName() {
		return this.userName;
	}

}

/**
 * 
 */
package com.gapsi.api.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.gapsi.api.model.dto.Supplier;
import com.gapsi.api.model.dto.response.BaseResponse;
import com.gapsi.api.model.dto.response.GetSuppliersResponse;
import com.gapsi.api.service.ISupplierService;

import lombok.extern.log4j.Log4j2;

/**
 * @author jereyesn
 *
 */
@Path("/suppliers")
@Log4j2
public class SupplierEndpoint {

	@Autowired
	private ISupplierService supplierService;

	public SupplierEndpoint() {
		log.debug(String.format("%s created!!!", this.getClass().getName()));
	}

	@GET
	@Path("/getSuppliers")
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponse<GetSuppliersResponse> getSuppliers() {
		log.debug("Request service received...");
		return supplierService.getAllSuppliers();
	}

	@GET
	@Path("/getData")
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponse<GetSuppliersResponse> getData() {
		log.debug("Request service received...");
		return supplierService.getAllSuppliers();
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponse<String> addSupplier(Supplier supplier) {
		log.debug("Request service received...");
		return supplierService.addSupplier(supplier);
	}

	@PUT
	@Path("/edit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponse<String> editSupplier(Supplier supplier) {
		log.debug("Request service received...");
		return supplierService.editSupplier(supplier);
	}

	@DELETE
	@Path("/delete/{supplierId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponse<String> deleteSupplier(@PathParam("supplierId") long supplierId) {
		log.debug("Request service received...");
		return supplierService.deleteSupplier(supplierId);
	}

}

/**
 * 
 */
package com.gapsi.api.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import com.gapsi.api.model.dto.response.AppVersionResponse;
import com.gapsi.api.model.dto.response.BaseResponse;
import com.gapsi.api.model.dto.response.UserResponse;
import com.gapsi.api.service.IDataService;

import lombok.extern.log4j.Log4j2;

/**
 * @author jereyesn
 *
 */
@Path("/data")
@Log4j2
public class DataEndpoint {

	@Autowired
	private IDataService dataService;

	public DataEndpoint() {
		log.debug(String.format("%s created!!!", this.getClass().getName()));
	}

	@GET
	@Path("/version")
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponse<AppVersionResponse> getAppVersion() {
		log.debug("Request received...");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException exception) {
			log.error(exception);
		}
		return dataService.getAppVersion();

	}

	@GET
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponse<UserResponse> getUser() {
		log.debug("Request received...");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException exception) {
			log.error(exception);
		}
		return dataService.getUserName();

	}

}

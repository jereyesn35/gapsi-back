# Gapsi Back

## Name
Gapsi e-commerce back end.

## Getting started

Como prerrequisito, se debe tener instaladas las siguientes dependendencias para compilar el proyecto.

JDK 1.8
Gradle 6.7+

Ejecutar los siguientes pasos para iniciar la aplicación por medio de Spring boot

1. Sobre la raiz del proyecto (gapsi-app-api), ejecutar el comando gradle build
2. Entrar a la carpeta build/libs/ y buscar el archivo gapsi-app-api-1.0.0-SNAPSHOT.jar
3. En consola, ejecutar el comando java -jar gapsi-app-api-1.0.0-SNAPSHOT.jar

Si no tiene ninguna excepcion en consola, el proyecto se estara ejecutando correctamente en el puerto 9000

## Testing API

En la raiz del repositorio hay una coleccion de Postman para probar los servicios creados para el proyecto.